package com.homework.lab3.communicator

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class ReceiveMessageActivity : AppCompatActivity() {

    private lateinit var getResultButton : Button

    companion object {
        @JvmStatic val RESULT = "RESULT"
        @JvmStatic val EXTRA_MESSAGE = "message"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receive_message)
        val receivedMessageText = intent.getStringExtra(EXTRA_MESSAGE)
        val textView = findViewById<TextView>(R.id.textView)
        textView.text = receivedMessageText
        getResultButton = findViewById(R.id.button2)
        getResultButton.setOnClickListener(getResultButtonListener);
    }

    private val getResultButtonListener = View.OnClickListener {
        val intent = Intent().putExtra(Intent.EXTRA_RETURN_RESULT, RESULT)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
