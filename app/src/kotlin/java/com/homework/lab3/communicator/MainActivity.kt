package com.homework.lab3.communicator

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    private lateinit var mainInput: EditText

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        loadExtraResult(data)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainInput = findViewById(R.id.edittext)
        val sendButton = findViewById<Button>(R.id.button)
        sendButton.setOnClickListener(sendButtonClickListener)
    }

    private fun loadExtraResult(data: Intent?) {
        val result = data?.getStringExtra(Intent.EXTRA_RETURN_RESULT)
        mainInput.setText(result)
    }

    private val sendButtonClickListener = View.OnClickListener {
        val mainInputMessage = mainInput.text.toString()
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, mainInputMessage)
        val chooserTitle = getString(R.string.chooser)
        val chooserIntent = Intent.createChooser(intent, chooserTitle)
        try {
            startActivity(chooserIntent)
        } catch (ex: ActivityNotFoundException) {
            ex.printStackTrace()
        }
    }
}
