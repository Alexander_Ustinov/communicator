package com.homework.lab3.communicator;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText mainInput;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loadExtraResult(data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainInput = findViewById(R.id.edittext);
        Button sendButton = findViewById(R.id.button);
        sendButton.setOnClickListener(getSendButtonClickListener());
    }

    private void loadExtraResult(Intent data) {
        String result = data.getStringExtra(Intent.EXTRA_RETURN_RESULT);
        mainInput.setText(result);
    }

    private View.OnClickListener getSendButtonClickListener() {
        return button -> {
            String message = mainInput.getText().toString();
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, message);
            String chooserTitle = getString(R.string.chooser);
            Intent chooserIntent = Intent.createChooser(intent, chooserTitle);
            try {
                startActivity(chooserIntent);
            } catch (ActivityNotFoundException ex) {
                ex.printStackTrace();
            }
        };
    }
}
