package com.homework.lab3.communicator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ReceiveMessageActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "message";
    private static final String RESULT = "RESULT";

    private Button getResultButton = findViewById(R.id.button2);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_message);
        String receivedMessageText = getIntent().getStringExtra(EXTRA_MESSAGE);
        TextView textView = findViewById(R.id.textView);
        textView.setText(receivedMessageText);
        getResultButton.setOnClickListener(getResultButtonClickListener());
    }

    private View.OnClickListener getResultButtonClickListener() {
        return v -> {
            Intent intent = new Intent();
            intent.putExtra(Intent.EXTRA_RETURN_RESULT, RESULT);
            setResult(RESULT_OK, intent);
            finish();
        };
    }
}
